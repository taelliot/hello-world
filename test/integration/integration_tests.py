import requests

def test_get_hello():
    r = requests.get("http://localhost:3000/")
    assert(r.text == 'Welcome!\n')

def test_get_greet_no_name_returns_404():
    r = requests.get("http://localhost:3000/greet")
    assert(r.status_code == 404)

def test_get_greet_name_returns_200_and_greeting():
    r = requests.get("http://localhost:3000/greet/tester")
    assert(r.text == 'Hi, tester!\n')

def test_get_metrics_returns_200():
    r = requests.get("http://localhost:3000/metrics")
    assert(r.status_code == 200)

def test_post_hello_returns_404():
    r = requests.post("http://localhost:3000/")
    assert(r.status_code == 404)

def test_post_greet_no_name_returns_404():
    r = requests.post("http://localhost:3000/greet")
    assert(r.status_code == 404)

def test_post_greet_name_returns_404():
    r = requests.post("http://localhost:3000/greet/tester")
    assert(r.status_code == 404)

def test_post_metrics_returns_404():
    r = requests.post("http://localhost:3000/metrics")
    assert(r.status_code == 404)
