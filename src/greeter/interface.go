package greeter

// Greeter defines how we say hello
type Greeter interface {
	Greet() string
	SayHi(name string) string
}
