package factory

import (
	"fmt"
	
	greeter "bitbucket.org/taelliot/hello-world/src/greeter"
	english "bitbucket.org/taelliot/hello-world/src/greeter/english"
)

func New(language string) (greeter.Greeter, error) {
	var greeter greeter.Greeter
	switch language {
	case "english":
		return english.New(), nil
	}
	return greeter, fmt.Errorf("No greeter found called {0}", language)
}
