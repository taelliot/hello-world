package english

import (
	"fmt"

	greeter "bitbucket.org/taelliot/hello-world/src/greeter"
)

type english struct {}

func New() greeter.Greeter {
	return &english{}
}

func (e english) Greet() string {
	return "Welcome!"
}

func (e english) SayHi(name string) string {
	return fmt.Sprintf("Hi, %s!", name)
}
