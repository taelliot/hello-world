package server

import (
	"net/http"

	g "bitbucket.org/taelliot/hello-world/src/greeter"
	"github.com/gorilla/mux"
)

type Route struct {
	Name        string
	Method      string
	Pattern     string
	HandlerFunc http.HandlerFunc
}

type Routes []Route

var greeter g.Greeter

func NewRouter(l g.Greeter) *mux.Router {
	greeter = l
	router := mux.NewRouter().StrictSlash(true)
	for _, route := range routes {
		var handler http.Handler
		handler = route.HandlerFunc
		handler = Logger(handler, route.Name)
		router.
			Methods(route.Method).
			Path(route.Pattern).
			Name(route.Name).
			Handler(handler)
	}
	return router
}

var routes = Routes{
	Route{
		"Index",
		"GET",
		"/",
		Index,
	},
	Route{
		"Greet",
		"GET",
		"/greet/{name}",
		Greet,
	},
	Route{
		"Metrics",
		"GET",
		"/metrics",
		Metrics,
	},
}
