package server

import (
	"fmt"
	mux "github.com/gorilla/mux"
	"github.com/prometheus/client_golang/prometheus"
	"net/http"
)

func Index(w http.ResponseWriter, r *http.Request) {
	fmt.Fprintln(w, greeter.Greet())
	w.Header().Set("Content-Type", "plaintext; charset=UTF-8")
}

func Greet(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	name := vars["name"]
	fmt.Fprintln(w, greeter.SayHi(name))
	w.Header().Set("Content-Type", "plaintext; charset=UTF-8")
}

func Metrics(w http.ResponseWriter, r *http.Request) {
	go prometheus.Handler().ServeHTTP(w, r)
}
