package main

import (
	"log"
	"net/http"

	factory "bitbucket.org/taelliot/hello-world/src/greeter/factory"
	server "bitbucket.org/taelliot/hello-world/src/server"
)

type greetRequest struct {
	Name string
}

func main() {
	eng, err := factory.New("english")
	if err != nil {
		log.Fatal(err)
	}
	router := server.NewRouter(eng)
	http.ListenAndServe(":3000", router)
}
