#!/bin/bash

docker rm -f lt-greeter > /dev/null
docker rm -f lt-locust > /dev/null

docker build -t greeter .

docker run --name lt-greeter -d greeter
docker run --name lt-locust -d -p 31337:8089 -v "$(pwd)"/tests/load:/tests --link lt-greeter:greeter locust -f /tests/locustfile.py --host=http://greeter:3000

# let locust start up
sleep 3
test_time=180 #seconds
locust_count=100
hatch_rate=10

curl -s -X POST -d "locust_count=${locust_count}&hatch_rate=${hatch_rate}" http://localhost:31337/swarm > /dev/null
echo "Load test can be manually intervened from http://localhost:31337."
echo "Terminating in ${test_time} seconds."

sleep ${test_time}

mkdir -p "$(pwd)"/tests/load/results
curl localhost:31337/stats/requests/csv > "$(pwd)"/tests/load/results/statistics.csv
curl localhost:31337/stats/distribution/csv > "$(pwd)"/tests/load/results/distribution.csv
curl localhost:31337/exceptions > "$(pwd)"/tests/load/results/exceptions.txt

docker rm -f lt-greeter > /dev/null
docker rm -f lt-locust > /dev/null
