from locust import HttpLocust, TaskSet, task

class WebsiteTasks(TaskSet):
    @task(1)
    def index(self):
      self.client.get("/")

class WebsiteUser(HttpLocust):
    task_set = WebsiteTasks
    min_wait = 5
    max_wait = 10
