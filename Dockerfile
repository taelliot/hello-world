FROM scratch
ADD bin/hello-world /
EXPOSE 3000
CMD ["/hello-world"]
