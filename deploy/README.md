# Hello World Deployment Demo

# v1 Basics
This will make a simple pod that no one can really use.
```
# Create the pod
kubctl create -f v1
```

# v2 More Useful
This will make the same pod, but expose a [ClusterIP](http://kubernetes.io/docs/user-guide/services/#publishing-services---service-types) service that allows other pods to use it. There is no way to access these resources from outside of the cluster...yet!

*Create the service and pod*
```
kubectl create -f v2
# look at the service info
kubectl describe svc hello-world-service
```

*Check the ns lookup for our service*
```
kubectl run -it curlpod --image=radial/busyboxplus:curl --namespace=demo

# hit enter to go into the container
nslookup hello-world-service

# let's hit it
curl hello-world-service
```

You can reference `hello-world-service` from anywhere in your namespace and it will resolve to that pod.

# v3 Getting More Serious

Make it so we can talk to our pod from the outside world.

```
kubectl create -f v3
watch kubctl describe svc hello-world-service
# wait for an "external-ip" to show up
# go to aws and watch for your elb health checks 
# to pass
```

# v4 Real World
We saw that we only have 1 of our 2 instances in service - we need more pods!

```
kubectl create -f v4
# do the same song and dance as above
# now let's scale
kubectl scale rc hello-world-rc --replicas=10
```

# v5 More Glitz
Let's add a route53 entry. This uses a [plugin for Kubernetes](https://github.com/wearemolecule/route53-kubernetes) that reads annotation data and performs a job.

```
kubectl create -f v5
```
